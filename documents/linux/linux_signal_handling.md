#linux signal handling

 Linux 등에서 daemon을 작성하기 위해서는 기본적으로 signal 처리에 대해서 잘 알아야 한다.
그러나, 나는 기존에 해둔 코드를 copy & paste 만 전문적으로 했기 때문에, 실제 signal hanling에 
대해서는 잘 모르는데...

 이번 문서에서 확실하게 알아야 할 것은 두 가지이다.
  * 어떤 signal을 어떻게 처리해야 하는가?
  * multi threaded 환경에서 signal을 어떻게 처리해야 하는가?

 먼저 결론부터 다시 정리하자면...
  * signal은 옵션에 따라 같은 signal이 여러개 올 수도 있지만, 기본적으로 같은 signal은 한번만 
   동시에 수행된다. - 끝나기 전에 같은 signal이 다시 오지 않는다.
  * 같은 signal은 동시에 오지 않아도, 같은 signal handler로 설정하면 여러번 수행이 가능하다.
  * 이 때 서로 다른 thread 일 수도 있고, 같은 thread 일 수도 있다.

  1번의 경우에는 실제로 꽤 많은 책에서 내용이 있지만, 2번은 거의 없으므로 해당 부분에 대해서 알아보자.

## 리눅스 커널에서 linux signal handing

 먼저 확인하고자 하는 함수는 kernel/signal.c 에 있는 complete_signal 함수이다.
 아직 솔직히 언제 호출되는지는 잘 모르지만... 느낌적인 느낌상, signal을 처리할 thread를 지정하는 함수로
보인다.(예비 지식이 있어서 다행인가...)

 먼저 넘어오는 파라매터에 signal 종류와 task_struct *p 구조체가 있다.
주석을 보면 main thread 가 signal을 원하면 할당한다라는 내용이 있으니, 아마도 p는 
main thread 에 대한 정보를 가지고 있을 것이라고 추측할 수 있다.

 thread가 하나고, 깨울 필요가 없으면 해당 signal은 무시한다.
마지막으로 현재 signal 에 설정된 current target thread 부터 체크해서, 해당 signal을 처리할
생각이 없다면, 무시한다. 

 즉, singal handler는 여러 thread에서 호출될 수 있다.(동시에 돌 수 있는가는? 모르겠음.)

```
#!c
static void complete_signal(int sig, struct task_struct *p, int group)
{
    struct signal_struct *signal = p->signal;
    struct task_struct *t;

    /*
     * Now find a thread we can wake up to take the signal off the queue.
     *
     * If the main thread wants the signal, it gets first crack.
     * Probably the least surprising to the average bear.
     */
    if (wants_signal(sig, p))
        t = p;
    else if (!group || thread_group_empty(p))
        /*
         * There is just one thread and it does not need to be woken.
         * It will dequeue unblocked signals before it runs again.
         */
        return;
    else {
        /*
         * Otherwise try to find a suitable thread.
         */
        t = signal->curr_target;
        while (!wants_signal(sig, t)) {
            t = next_thread(t);
            if (t == signal->curr_target)
                /*
                 * No thread needs to be woken.
                 * Any eligible threads will see
                 * the signal in the queue soon.
                 */
                return;
        }
        signal->curr_target = t;
    }

    /*
     * Found a killable thread.  If the signal will be fatal,
     * then start taking the whole group down immediately.
     */
    if (sig_fatal(p, sig) &&
        !(signal->flags & (SIGNAL_UNKILLABLE | SIGNAL_GROUP_EXIT)) &&
        !sigismember(&t->real_blocked, sig) &&
        (sig == SIGKILL || !t->ptrace)) {
        /*
         * This signal will be fatal to the whole group.
         */
        if (!sig_kernel_coredump(sig)) {
            /*
             * Start a group exit and wake everybody up.
             * This way we don't have other threads
             * running and doing things after a slower
             * thread has the fatal signal pending.
             */
            signal->flags = SIGNAL_GROUP_EXIT;
            signal->group_exit_code = sig;
            signal->group_stop_count = 0;
            t = p;
            do {
                task_clear_jobctl_pending(t, JOBCTL_PENDING_MASK);
                sigaddset(&t->pending.signal, SIGKILL);
                signal_wake_up(t, 1);
            } while_each_thread(p, t);
            return;
        }
    }

    /*
     * The signal is already in the shared-pending queue.
     * Tell the chosen thread to wake up and dequeue it.
     */
    signal_wake_up(t, sig == SIGKILL);
    return;
}
```

 실제로 wants_signal 함수는 다음과 같다. PF_EXITING은 shutdown 중인 process나 thread
이므로, 더 signal을 처리할 필요가 없다.

```
#!c
/*
 * Test if P wants to take SIG.  After we've checked all threads with this,
 * it's equivalent to finding no threads not blocking SIG.  Any threads not
 * blocking SIG were ruled out because they are not running and already
 * have pending signals.  Such threads will dequeue from the shared queue
 * as soon as they're available, so putting the signal on the shared queue
 * will be equivalent to sending it to one such thread.
 */
static inline int wants_signal(int sig, struct task_struct *p)
{
    if (sigismember(&p->blocked, sig))
        return 0;
    if (p->flags & PF_EXITING)
        return 0;
    if (sig == SIGKILL)
        return 1;
    if (task_is_stopped_or_traced(p))
        return 0;
    return task_curr(p) || !signal_pending(p);
}
```

 실제로 complete_signal 은 kernel/signal.c 에서 __send_signal, send_sigqueue
 두 군데서만 호출된다.

 이 문서의 핵심은, daemon 작성시에 signal을 어떻게 처리할 것인가 이므로, 이제 실 signal 처리 예를
살펴보기로 하자. 여기서는 redis, twemproxy, memcache에서 어떻게 처리하는지를 살펴보기로 한다.

 먼저 Redis 부터 살펴보도록 하자. Redis 는 src/server.c 의 initServer 함수에서 다음과 같이 시작한다.

```
#!c
 void initServer(void) {
    int j;

    signal(SIGHUP, SIG_IGN);
    signal(SIGPIPE, SIG_IGN);
    setupSignalHandlers();
    ......
}
```

  먼저 SIGHUP과 SIGPIPE는 무시한다. 그리고 setupSignalHandlers 함수에서 signalHandler를 
 설정한다.

```
#!c
 void setupSignalHandlers(void) {
    struct sigaction act;

    /* When the SA_SIGINFO flag is set in sa_flags then sa_sigaction is used.
     * Otherwise, sa_handler is used. */
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = sigShutdownHandler;
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGINT, &act, NULL);

    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_NODEFER | SA_RESETHAND | SA_SIGINFO;
    act.sa_sigaction = sigsegvHandler;
    sigaction(SIGSEGV, &act, NULL);
    sigaction(SIGBUS, &act, NULL);
    sigaction(SIGFPE, &act, NULL);
    sigaction(SIGILL, &act, NULL);
    return;
}
```

 코드를 보면 아주 간단합니다. sigShutdownHandler 함수가 signal handler로 지정되고 
SIGTERM 과 SIGINT 일 때 처리하게 됩니다. HAVE_BACKTRACE가 지정되면 좀 더 작업이 발생합니다.
이 부분은 시스템 프로그래밍 서적을 참고하세요. 

```
#!c
static void sigShutdownHandler(int sig) {
    char *msg;

    switch (sig) {
    case SIGINT:
        msg = "Received SIGINT scheduling shutdown...";
        break;
    case SIGTERM:
        msg = "Received SIGTERM scheduling shutdown...";
        break;
    default:
        msg = "Received shutdown signal, scheduling shutdown...";
    };

    /* SIGINT is often delivered via Ctrl+C in an interactive session.
     * If we receive the signal the second time, we interpret this as
     * the user really wanting to quit ASAP without waiting to persist
     * on disk. */
    if (server.shutdown_asap && sig == SIGINT) {
        serverLogFromHandler(LL_WARNING, "You insist... exiting now.");
        rdbRemoveTempFile(getpid());
        exit(1); /* Exit with an error since this was not a clean shutdown. */
    } else if (server.loading) {
        exit(0);
    }

    serverLogFromHandler(LL_WARNING, msg);
    server.shutdown_asap = 1;
}
```

 Redis 는 거의 single threaded application 이므로, 거의 여기서 그냥 코드가 끝나버립니다. 
signal handler로 넘어오는 것들은 전부 종료하거나, rdb를 생성하고 종료합니다.

 이제 twemproxy를 살펴봅시다. twemproxy에서는 먼저 처리할 signal 들에 대한 정의를 해두었습니다.
```
#!c
static struct signal signals[] = {
    { SIGUSR1, "SIGUSR1", 0,                 signal_handler },
    { SIGUSR2, "SIGUSR2", 0,                 signal_handler },
    { SIGTTIN, "SIGTTIN", 0,                 signal_handler },
    { SIGTTOU, "SIGTTOU", 0,                 signal_handler },
    { SIGHUP,  "SIGHUP",  0,                 signal_handler },
    { SIGINT,  "SIGINT",  0,                 signal_handler },
    { SIGSEGV, "SIGSEGV", (int)SA_RESETHAND, signal_handler },
    { SIGPIPE, "SIGPIPE", 0,                 SIG_IGN },
    { 0,        NULL,     0,                 NULL }
};
```

 그리고 twemproxy 에서 signal_init 함수에서 다음과 같이 설정합니다.
```
#!c
rstatus_t
signal_init(void)
{
    struct signal *sig;

    for (sig = signals; sig->signo != 0; sig++) {
        rstatus_t status;
        struct sigaction sa;

        memset(&sa, 0, sizeof(sa));
        sa.sa_handler = sig->handler;
        sa.sa_flags = sig->flags;
        sigemptyset(&sa.sa_mask);

        status = sigaction(sig->signo, &sa, NULL);
        if (status < 0) {
            log_error("sigaction(%s) failed: %s", sig->signame,
                      strerror(errno));
            return NC_ERROR;
        }
    }

    return NC_OK;
}
```

 그냥 위의 signals 배열을 돌면서 sig->handler와 sig->flags 로 설정합니다.
실제 signal handerl 인 signal_handler 를 살펴봅시다.

```
#!c
void signal_handler(int signo)
{
    struct signal *sig;
    void (*action)(void);
    char *actionstr;
    bool done;

    for (sig = signals; sig->signo != 0; sig++) {
        if (sig->signo == signo) {
            break;
        }
    }
    ASSERT(sig->signo != 0);

    actionstr = "";
    action = NULL;
    done = false;

    switch (signo) {
    case SIGUSR1:
        break;

    case SIGUSR2:
        break;

    case SIGTTIN:
        actionstr = ", up logging level";
        action = log_level_up;
        break;

    case SIGTTOU:
        actionstr = ", down logging level";
        action = log_level_down;
        break;

    case SIGHUP:
        actionstr = ", reopening log file";
        action = log_reopen;
        break;

    case SIGINT:
        done = true;
        actionstr = ", exiting";
        break;

    case SIGSEGV:
        log_stacktrace();
        actionstr = ", core dumping";
        raise(SIGSEGV);
        break;

    default:
        NOT_REACHED();
    }

    log_safe("signal %d (%s) received%s", signo, sig->signame, actionstr);

    if (action != NULL) {
        action();
    }

    if (done) {
        exit(1);
    }
}
```

 로그 레벨을 올리거나, 종료하거나 별 다른 action은 없습니다. 이것만 보면, 왜 signal이 어렵다고 생각했는지...
이제 최종적으로 memcached의 소스를 살펴봅도록 하겠습니다. 더 간단합니다. 그냥 종료합니다. 

```
#!c
 int main (int argc, char **argv) {
 	......
 	/* handle SIGINT and SIGTERM */
    signal(SIGINT, sig_handler);
    signal(SIGTERM, sig_handler);
	......
}

static void sig_handler(const int sig) {
    printf("Signal handled: %s.\n", strsignal(sig));
    exit(EXIT_SUCCESS);
}

```

 결론적으로 같은 signal은 기본적으로 연속적으로 받지 않지만, flags 를 SA_NODEFER 로 줄 경우에는
기본적으로 같은 signal도 동시에 처리가 가능하다. 위의 예제처럼 여러 signal을 하나의 signal handler
로 등록한 경우에는, 각 signal 에 따라서 signal handler가 동시에 처리될 수 있으므로, thread-safe
와 유사하게 작성해야 한다.(signal safe라고 한다.)

 SA_RESTART 옵션을 줄 경우, EINVAL이 기본적으로는 발생하지 않는다.(Timeout이 발생할 수 있는 경우 제외)